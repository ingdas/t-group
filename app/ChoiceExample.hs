{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase           #-}
{-# LANGUAGE OverloadedStrings    #-}
module ChoiceExample where

import           Data.Text             (Text)
import           Database.MongoDB
import           Guards
import           XSDM
import           XSDSpec
import           Control.Monad


example2 = tag "a" [
      choice [
          onlyIf possibleSequence1 ( multi [
              showNewAttribute "a_a" getAa,
              showNewAttribute "a_b" getAb
          ]),
          onlyIf possibleSequence2 ( multi [
              showNewAttribute "b_a" getBa,
              showNewAttribute "b_b" getBb
          ])
        ]
      ]

getAa = getAttrFromDb (exactly 1) "telephone" "root"
getAb = getAttrFromDb (exactly 1) "email" "root"
getBa = getAttrFromDb (exactly 1) "city" "root"
getBb = getAttrFromDb (exactly 1) "address" "root"
possibleSequence1 = (attributeExists <$> getAttrFromDb (atMost 1) "telephone" "root") <||> (attributeExists <$> getAttrFromDb (atMost 1) "email" "root")
possibleSequence2 = (attributeExists <$> getAttrFromDb (atMost 1) "city" "root") <||> (attributeExists <$> getAttrFromDb (atMost 1) "address" "root")

attributeExists = not.null
(<||>) = liftM2 (||)

getAttrFromDb :: GuardF -> Text -> String -> XSDM [String]
getAttrFromDb guardF attributeN idName = consistentWith (show attributeN) guardF $ do
      pid <- pathAttribute idName
      o <- doDBCall $ find (select ["id" =: pid] "person")
      return $ map (at attributeN) o
