module Main where

import           Control.Lens          (at, (^.))
import           Fadno.Xml.ParseXsd
import           Text.XML.Light.Output
import           XSD2Input
import           XSDSpec

curFile = "/home/ingmar/gitrepos/tgroup/xsd/WECH008_20163.xsd"

main = do
  s <- parseFile curFile
  print s
  --print (s ^. elements ^. at (qn "DclInformation"))
  let asInput = execConvert $ convert s "WECH008"
  case asInput of
    Left e -> do putStrLn "Error during conversion" ; print e
    Right inp -> do
        e <- dummyExecute inp
        case e of
          Left e  -> do putStrLn "Error during execution" ; print e
          Right l -> putStrLn . ppElement . head $ l

  --print (s  ^. simpleTypes)
  --print (s  ^. complexTypes)
