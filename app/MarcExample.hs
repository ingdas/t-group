{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase           #-}
{-# LANGUAGE OverloadedStrings    #-}

module Main where

import           ChoiceExample
import           Control.Lens          hiding (at)
import           Control.Monad.Trans   (liftIO)
import qualified Data.Map              as M
import           Data.Text             (Text)
import           Database.MongoDB
import           Guards
import           Text.XML.Light.Output
import           XSDM
import           XSDSpec

main = do
   putStrLn "Go:"
   pipe <- connect (host "127.0.0.1")
   let dBAccessor = access pipe master "t-group"
   dBAccessor initialize
   print =<< test dBAccessor example
   close pipe

test dbA ex = output =<< (execXSDM (RS initDb dbA) . fmap head . execute) ex
 where
   initDb = M.fromList [("root","1")]
   output f = case f of
     (Left a)  -> putStrLn $ "Error: " ++ show a
     (Right b) -> putStrLn $ ppElement b

--Building a database

initialize = do
  delete (select [] "person")
  insertMany "person" [
      ["id" := String "1",            "name" =: "ingmar",   "street" =: "Celestijnenlaan",  "city" =: "Heverlee", "gender" := String "m"],
      ["id" := String "2",            "name" =: "alice",    "street" =: "Celestijnenlaan",  "city" =: "Heverlee", "gender" := String "v"],
      ["id" := String "3",            "name" =: "single",   "street" =: "Celestijnenlaan",  "city" =: "Heverlee", "gender" := String "v"],
      ["id" := String "TwoNames",     "name" =: "ingzmar",  "street" =: "Celestijnenlaan",  "city" =: "Heverlee", "gender" := String "m"],
      ["id" := String "TwoNames",     "name" =: "alice",    "street" =: "Bongenotenlaan",   "city" =: "Leuven",   "gender" := String "v"],
      ["id" := String "OtherGender",  "name" =: "os",       "street" =: "Bongenotenlaan",   "city" =: "Leuven",   "gender" := String "o"],
      ["id" := String "NoPhone",      "name" =: "os",       "street" =: "Bongenotenlaan",   "city" =: "Leuven",   "gender" := String "o"],
      ["id" := String "EmailAndPhone","name" =: "os",       "street" =: "Bongenotenlaan",   "city" =: "Leuven",   "gender" := String "m"]
      ]
  delete (select [] "marriage")
  insertMany "marriage" [
      ["husband" := String "1", "wife" := String "2"]
      ]
  delete (select [] "phones")
  insertMany "phones" [
      ["id" := String "1", "telephone" =: "911"],
      ["id" := String "1", "telephone" =: "912"],
      ["id" := String "1", "telephone" =: "913"],
      ["id" := String "2", "telephone" =: "913"],
      ["id" := String "3", "telephone" =: "913"],
      ["id" := String "TwoNames", "telephone" =: "913"],
      ["id" := String "OtherGender", "telephone" =: "913"],
      ["id" := String "EmailAndPhone", "telephone" =: "913"]
      ]
  delete (select [] "emails")
  insertMany "emails" [
      ["id" := String "EmailAndPhone", "email" := String "a@b.com"]
      ]

-- Actual TestCase
example = tag "personinfo" [
               showNewAttribute "name" (getNameOf "root"),
               showNewAttribute "address" (getStreetOf "root"),
               showNewAttribute "city" (getCityOf "root"),
               choice [
                    onlyIf (hasPhone "root") $ tag "phones" [showNewAttribute "telepone" (getTelephoneOf "root")],
                    showNewAttribute "email" (getEmailOf "root")],
               showNewAttribute "gender"  (getGenderOf "root"),
               addAttribute "marriedWith" (getMarriedId "root") $
                    tag "significantOther" [
                        showNewAttribute "marriedName" (getNameOf "marriedWith")]
           ]

getNameOf :: String-> XSDM [String]
getNameOf = getAttrFromPerson (exactly 1) "name"

getStreetOf :: String-> XSDM [String]
getStreetOf = getAttrFromPerson (exactly 1) "street"

getCityOf :: String-> XSDM [String]
getCityOf = getAttrFromPerson (exactly 1) "city"

getGenderOf :: String-> XSDM [String]
getGenderOf = getAttrFromPerson (exactly 1 `andAlso` inDomain ["m","v"]) "gender"

getTelephoneOf :: String -> XSDM [String]
getTelephoneOf x = consistentWith "telephone" (between 1 3) $ do
  pid <- pathAttribute x
  o <- doDBCall $ find (select ["id" =: pid] "phones")
  return $ map (at "telephone") o

hasPhone :: String -> XSDM Bool
hasPhone x = (not.null) <$> getTelephoneOf x

getEmailOf :: String -> XSDM [String]
getEmailOf x = consistentWith "emails" unrestricted $ do
  pid <- pathAttribute x
  o <- doDBCall $ find (select ["id" =: pid] "emails")
  return $ map (at "email") o

getMarriedId :: String -> XSDM [String]
getMarriedId x = consistentWith "marriedId" (atMost 1) $ do
  pid <- pathAttribute x
  getGenderOf x >>= \case
            ["m"] -> fmap (at "wife") <$> doDBCall (find (select ["husband" =: pid] "marriage"))
            ["v"] -> fmap (at "husband") <$> doDBCall (find (select ["wife" =: pid] "marriage"))

getAttrFromPerson :: GuardF -> Text -> String -> XSDM [String]
getAttrFromPerson guardF attributeN idName = consistentWith (show attributeN) guardF $ do
      pid <- pathAttribute idName
      o <- doDBCall $ find (select ["id" =: pid] "person")
      return $ map (at attributeN) o
