module Guards where

import           Control.Lens
import           Control.Monad
import           Control.Monad.Except
import           Error
import           XSDM

type GuardF =  String -> [String] -> XSDM ()

--Guard Combinators
consistentWith :: String -> GuardF -> XSDM [String] -> XSDM [String]
consistentWith a f x = do
  vx <- x
  f a vx
  return vx

andAlso :: GuardF -> GuardF -> GuardF
andAlso f1 f2 a ls = f1 a ls <* f2 a ls

--Primitive Guards
exactly :: Int -> GuardF
exactly n a ls = atLeast n a ls <* atMost n a ls

atLeast :: Int -> GuardF
atLeast n a ls = when (length ls < n) $ throwError . TooFew a (length ls) n =<< view rsPath

atMost :: Int -> GuardF
atMost n a ls = when (length ls > n) $ throwError . TooMany a (length ls) n =<< view rsPath

between :: Int -> Int -> GuardF
between a b = atLeast a `andAlso` atMost b

inDomain :: [String] -> GuardF
inDomain dom a vals = unless (null illegals) (throwError . UnexpectedValue a (head illegals) dom =<< view rsPath)
      where illegals = filter (not . (`elem` dom)) vals

unrestricted :: GuardF
unrestricted _ _ = return ()
