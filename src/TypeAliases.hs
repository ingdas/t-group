module TypeAliases (
    module Data.Map,
    Path,
    DBAccessor
) where

import           Data.Map         (Map)
import           Database.MongoDB


type Path = Map String String
type DBAccessor = Action IO [Document] -> IO [Document]
