{-# LANGUAGE RankNTypes    #-}
{-# LANGUAGE TupleSections #-}

module XSDSpec where

import           Control.Lens
import           Control.Monad
import           Control.Monad.Except
import           Control.Monad.Reader
import qualified Data.Map             as M
import           Database.MongoDB
import           Error
import           Guards
import           Text.XML.Light
import           TypeAliases
import           XSDM

-- Input/Output Declaration
data Input = ILeaf String (XSDM String)
            |ITag String Input
            |INode (XSDM [(Input, Path)])
            |IChoice [Input]

dummyExecute :: Input -> IO (Either Error [Element])
dummyExecute = execXSDM (RS M.empty (const $ return []) M.empty) . f
  where
    f :: Input -> XSDM [Element]
    f (ILeaf s i) = sequence  [unode s <$> i]
    f (ITag s i) =  sequence  [unode s <$> f i]
    f (INode xs) =  concat <$> (mapM (\(a,b) -> local (rsPath .~ b) $ f a) =<< xs)
    f (IChoice xs) = sequence [unode "choice" . concat <$> mapM f xs]

execute :: Input -> XSDM [Element]
execute (ILeaf s leaf) = sequence [unode s <$> leaf]
execute (ITag s i) = (:[]) <$> (unode s <$> execute i)
execute (INode i) = concat <$> (mapM execTuple =<< i)
execute (IChoice xs) = do
    os <- filter (not.null) <$> mapM execute xs
    when (length os>1) (throwError . WrongChoice os =<< view rsPath)
    return (head os)

execTuple :: (Input, Path) -> XSDM [Element]
execTuple (a,b) = local (rsPath .~ b) $ execute a

-- Input Construction
tag :: String -> [Input] -> Input
tag n i = ITag n $ INode  (zip i <$> (repeat <$> view rsPath))

addAttribute :: String -> XSDM [String] -> Input -> Input
addAttribute s f i = INode (mapM newPaths =<< f)
    where
        newPaths :: String -> XSDM (Input,Path)
        newPaths x = (i,) . M.insert s x <$> view rsPath

showAttribute :: String -> Input
showAttribute s = ILeaf s ((M.! s) <$> view rsPath)

showNewAttribute :: String -> XSDM [String] -> Input
showNewAttribute s f = addAttribute s f (showAttribute s)

choice :: [Input] -> Input
choice = IChoice

multi :: [Input] -> Input
multi xs = INode $ mapM (\x -> (x,) <$> view rsPath) xs

onlyIf :: XSDM Bool -> Input -> Input
onlyIf b r = INode $ do
        b' <- b
        if b' then sequence [(,) r <$> view rsPath] else
                   return []
