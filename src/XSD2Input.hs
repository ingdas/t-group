{-# LANGUAGE FlexibleContexts #-}

module XSD2Input where

import           Control.Lens       (at, singular, (^.), _Just)
import qualified Data.Map           as M
import           Data.Maybe
import           Fadno.Xml.ParseXsd
import           XSDM               (XSDM, execQuery)
import           XSDSpec
import Control.Monad.State
import Data.Map (Map)
import           Control.Monad.Except

execConvert :: Convert a -> Either String a
execConvert = flip evalState M.empty . runExceptT

type Convert a = ExceptT String (State (Map String Int)) a
queryName :: String -> Convert String
queryName n = do
  s <- get
  let val = M.findWithDefault 0 n s
  put (M.insert n (val+1) s)
  return (n ++ show val)

queryAndTagAttribute n s = do
    qn <- queryName n
    addAttribute qn (execQuery qn) . tag n <$> s

convert :: Schema -> String -> Convert Input
convert s name = convertElem (s ^. elements ^. at (qn name) . singular _Just)
  where
    convertElem :: Element -> Convert Input
    convertElem e@ElementSimple{} = let tagname = e^.elementName.qLocal in
                                     showNewAttribute tagname <$> (execQuery <$> queryName  tagname)
    convertElem e@ElementComplex{} = queryAndTagAttribute (e^.elementName.qLocal) $
                                          sequence [ convertComplexType (e ^. singular elementComplex)]
    convertElem e@ElementRef{} = convertRefWith (e ^. singular elementRef) convertElem
    convertElem e@ElementType{} = queryAndTagAttribute (e^.elementName.qLocal) $
                                        sequence [convertRefWith (e ^.  singular elementType)
                                                (either convertComplexType convertSimpleType)]

    convertComplexType :: ComplexType -> Convert Input
    convertComplexType c@ComplexTypeCompositor{} = convertComposite (c ^. singular complexCompositor . singular _Just)
    convertComplexType c@ComplexTypeSimple{}     = convertSimpleContent (c ^. singular complexSimpleContent)
    convertComplexType c@ComplexTypeComplex{}    = convertComplexContent (c ^. singular complexComplexContent)

    convertComposite :: Compositor -> Convert Input
    convertComposite (CompositorGroup g)    = convertGroup g
    convertComposite (CompositorChoice g)   = convertChoice g
    convertComposite (CompositorSequence g) = convertSequence g

    convertGroup :: Group -> Convert Input
    convertGroup g@GroupChoice{} = convertChoice (g ^. singular groupChoice)
    convertGroup g@GroupSequence{} = convertSequence (g ^. singular groupSequence)
    convertGroup _ = throwError "Unknown Group type"

    convertChoice :: Choice -> Convert Input
    convertChoice s = choice <$> mapM convertParticle (s ^. choiceParticles)

    convertSequence :: Sequence -> Convert Input
    convertSequence s = multi <$> mapM convertParticle (s ^. sequenceParticles)

    convertParticle :: Particle -> Convert Input
    convertParticle (PartElement e)  = convertElem e
    convertParticle (PartGroup g)    = convertGroup g
    convertParticle (PartChoice c)   = convertChoice c
    convertParticle (PartSequence s) = convertSequence s

    convertSimpleContent :: SimpleContent -> Convert Input
    convertSimpleContent s = convertRefWith (s ^. simpleContentBase) convertSimpleType

    convertSimpleType :: SimpleType -> Convert Input
    convertSimpleType t = let tagname = t ^.simpleTypeName.singular _Just.qLocal in
                            showNewAttribute tagname <$> (execQuery <$> queryName tagname)

    convertComplexContent :: ComplexContent -> Convert Input
    convertComplexContent s = multi <$> sequence (
                                  [convertRefWith (s ^. complexContentBase) convertComplexType] ++
                                  [convertAttributes (s ^. complexContentAttributes)] ++
                                  maybeToList (fmap convertComposite (s ^. complexContentCompositor)))

    convertAttributes :: Attributes -> Convert Input
    convertAttributes s = multi <$> sequence (
                              map (\x -> throwError "Can't translate attributes") (s ^. attrsAttributes) ++
                              map (\x -> throwError "Can't translate attribute groups") (s  ^. attrsAttributeGroups))


    convertRefWith :: Resolvable (Ref a) => Ref a -> (a -> Convert Input) -> Convert Input
    convertRefWith a = f (resolve s a)
      where
        f (Resolved a b) f = f b
        f (Unresolved a) _ = throwError ("Unresolved Ref" ++ show a)--return $ tag "UNRESOLVEDREF" []
        f Final _          = throwError "Final Ref"
