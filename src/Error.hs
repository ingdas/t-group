module Error where

import           Text.XML.Light
import           Text.XML.Light.Output
import           TypeAliases

data Error = TooFew String Int Int Path
           | TooMany String Int Int Path
           | UnexpectedValue String String [String] Path
           | UnknownInPath String Path
           | WrongChoice [[Element]] Path
           | UnknownQuery String

instance Show Error where
  show (TooFew s a b p) = "Got " ++ show a ++ " values but expected at least "++ show b ++ " values, in attribute " ++ s ++ " with path " ++ show p
  show (TooMany s a b p) = "Got " ++ show a ++ " values but expected at most "++ show b ++ " values, in attribute " ++ s ++ " with path " ++ show p
  show (UnexpectedValue s a b p) = "Got value " ++ a ++ " but expected a value in "++ show b ++ ", in attribute " ++ s ++ " with path " ++ show p
  show (UnknownInPath s p) = "Got asked for value of " ++ s ++ " in path " ++ show p
  show (WrongChoice es p) = "Got these answers in a choice: " ++ concatMap (show . concatMap ppElement) es ++ ", expected exactly 1,  in path " ++ show p
  show (UnknownQuery q) = "Didn't find the referenced query to " ++ q
