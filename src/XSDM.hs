{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE LambdaCase            #-}
module XSDM where

import           Control.Lens
import           Control.Lens.TH
import           Control.Monad
import           Control.Monad.Except
import           Control.Monad.Reader
import qualified Data.Map             as M
import Data.Map (Map)
import           Database.MongoDB
import           Error
import           TypeAliases
import Data.List (intercalate)

newtype XSDM a = XSDM {unXSDM :: ExceptT Error (ReaderT ReadState IO) a}
  deriving (Monad, Applicative, Functor, MonadReader ReadState, MonadError Error, MonadIO)

data ReadState = RS {_rsPath :: Path, _dbCall :: DBAccessor, _queries :: Map String (XSDM [String]) }
makeLenses ''ReadState


execQuery :: String -> XSDM [String]
--execQuery x = M.findWithDefault (throwError (UnknownQuery x)) x =<< view queries
execQuery x = M.findWithDefault defaultVal x =<< view queries
  where
    defaultVal = sequence [concat <$> sequence [return "Query ",return x, return " using ", intercalate ", " . M.keys <$> view rsPath]]

execXSDM :: ReadState -> XSDM a -> IO (Either Error a)
execXSDM a = flip runReaderT a . runExceptT . unXSDM


pathAttribute :: String -> XSDM String
pathAttribute x = M.lookup x <$> view rsPath >>= \case
                                        Nothing -> throwError =<< UnknownInPath x <$> view rsPath
                                        Just a -> return a


doDBCall :: ReaderT MongoContext IO Cursor -> XSDM [Document]
doDBCall qry = do
  c <- view dbCall
  (liftIO . c $ rest =<< qry) :: XSDM [Document]
